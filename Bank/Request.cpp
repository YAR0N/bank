#include "Request.h"
#include "Managers/BaseManager.h"
#include "Managers/AccountManager.h"

bool Request::parseRequest(const ssize_t& n, const char* data)
{
    std::string dataStr(data, n);
    std::regex commaR("\\,");
    std::regex requestPairR("^([^:,]+):([^:,]+)$");

    std::regex_token_iterator<std::string::iterator> empty;
    std::regex_token_iterator<std::string::iterator> commaI(dataStr.begin(), dataStr.end(), commaR, -1);

    std::smatch match;
    while (commaI != empty) {
        if(std::regex_match(commaI->str(), match, requestPairR) && match.size() == 3) {
           std::string requestP(match[1].str());
           std::string dataP(match[2].str());
            _requestData.insert(std::make_pair(requestP, dataP));
        }
        ++commaI;
    }
    return !_requestData.empty();
}

bool Request::getRequestParameter(const std::string& parameter, std::string& result) const{
    std::map<std::string, std::string>::const_iterator it = _requestData.find(parameter);
    if(it != _requestData.end()) {
        result = it->second;
        return true;
    } else {
        return false;
    }
}

Request* Request::setResponseParameter(const std::string parameter, const std::string &data) {
    _responseData.insert(std::make_pair(parameter, data));
}

void Request::formError(const std::string &reason) {
    _responseData.insert(std::make_pair(std::string("success"), std::string("0")));
    _responseData.insert(std::make_pair(std::string("error"), reason));
}

void Request::formSuccess() {
    _responseData.insert(std::make_pair(std::string("success"), std::string("1")));
}

const ssize_t Request::formResponceString(char** st) const {
    std::string res;
    for (auto& item: _responseData) {
        res.append(item.first+":"+item.second+",");
    }
    *st = new char[res.length()];
    return res.copy(*st, res.length());
}

const ssize_t Request::getResponce(char** st) {
    BaseManager* m = 0;
    std::string module;
    if (getRequestParameter("component", module)) {
       if(module == "Accounts") {
           m = new AccountManager(*this);
           m->makeResponce();
       } else
           formError("No such component");
    } else {
        formError("No component parameter");
    }

    return formResponceString(st);
}
