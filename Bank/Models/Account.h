#include <bits/stringfwd.h>
#include "BaseModel.h"

#ifndef ACCOUNT_H
#define ACCOUNT_H

class Account: public BaseModel<Account>{

public:
    Account()
            : _number(),
              _pin(),
              _clientName(),
              _amount(0) {
    }


    Account(std::string number, std::string pin, std::string clientName, double amount)
            : _number(number),
              _pin(pin),
              _clientName(clientName),
              _amount(amount) {
    }

    ~Account(){}

    Account* findByNumber(const std::string& number);

    const std::string &get_number() const {
        return _number;
    }

    Account* set_number(std::string &_number) {
        _number = _number;
        return this;
    }

    const std::string &get_pin() const {
        return _pin;
    }

    Account* set_pin(std::string &_pin) {
        _pin = _pin;
        return this;
    }

    const std::string &get_clientName() const {
        return _clientName;
    }

    Account* set_clientName(std::string &_clientName) {
        _clientName = _clientName;
        return this;
    }

    const double get_amount() const {
        return _amount;
    }

    Account* set_amount(double amount) {
        _amount = amount;
        return this;
    }

private:

    Account(const Account&);

    int _id;
    std::string _number;
    std::string _pin;
    std::string _clientName;
    double _amount;

    virtual bool doSave() const;

    static const std::string findSQL;
    static const std::string insertSQL;
    static const std::string updateSQL;

    virtual bool doUpdate() const;
};



#endif


