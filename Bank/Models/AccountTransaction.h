#include <bits/stringfwd.h>
#include <vector>
#include "BaseModel.h"
#include "../Request.h"

#ifndef ATRANSACTION_H
#define ATRANSACTION_H

class AccountTransaction: public BaseModel<AccountTransaction>{

public:

    AccountTransaction()
            : _numberFrom(),
              _numberTo(),
              _date(),
              _amount(0) {
    }

    AccountTransaction(std::string numberFrom, std::string numberTo, std::string date, double amount)
            : _numberFrom(numberFrom),
              _numberTo(numberTo),
              _date(date),
              _amount(amount) {
    }


     ~AccountTransaction(){}

    void findAllByAccountAddToResponce(const std::string& number, Request& req) const;

private:
    int _id;
    std::string _numberFrom;
    std::string _numberTo;
    std::string _date;
    double _amount;

    virtual bool doSave() const;
    virtual bool doUpdate() const;

    static const std::string insertSQL;
    static const std::string findAllSQL;

};



#endif


