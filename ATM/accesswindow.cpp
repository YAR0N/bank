#include "accesswindow.h"
#include "ui_accesswindow.h"
#include <QMessageBox>
#include <QInputDialog>
#include <QPropertyAnimation>
#include <QStringListModel>

accessWindow::accessWindow(const QString& ip, const quint16 &port, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::accessWindow),
    socket(new ATMSocket(ip,port)),
    _cardNum(""),
    _pin("")
{
    ui->setupUi(this);
    ui->accessPin->hide();
    ui->main->hide();
    ui->showBalance->hide();
    ui->sendMoney->hide();
    ui->History->hide();
    ui->GetMoneyWindow->hide();

    QRegExp *regExFloat = new QRegExp("^\\d+(?:\\.\\d{1,2})?$");
    ui->transfAmount->setValidator( new QRegExpValidator(*regExFloat) );
}

accessWindow::~accessWindow()
{
    delete ui;
}

void accessWindow::parseAnswer(const QByteArray& answer){
    QList<QByteArray> param = answer.split(',');
    QList<QByteArray> pairs;
    for(int i=0; i < param.length()-1; ++i){
        pairs = param[i].split(':');
        _answer.insert(pairs[0], pairs[1]);
    }
}

bool accessWindow::isSuccessed(){
    if(_answer.find("success").value() == "0"){
        showError(_answer.find("error").value());
        return false;
    }
    return true;
}

void accessWindow::showError(const QString& text){
      QMessageBox msgBox;
      msgBox.critical(this,"Error",text);
}


void accessWindow::on_pushButton_2_clicked()
{
    _cardNum = ui->Card1->text().replace(" ","");
    if(_cardNum.isEmpty() || _cardNum.length() < 16){
        showError("Imposible card number!");
        return;
    }
    ui->accessCard->hide();
    ui->pinCode->setFocus();
    ui->accessPin->show();
    ui->Card1->clear();
}


void accessWindow::on_pushButton_clicked()
{
    _pin = "";
    _pin = ui->pinCode->text();
    if(_pin.isEmpty() || _pin.length() < 4){
        showError("Imposible pin!");
        return;
    }
    ui->accessPin->hide();
    ui->main->show();
    ui->pinCode->clear();
}

void accessWindow::on_exit_clicked()
{
    ui->accessPin->hide();
    ui->main->hide();
    ui->showBalance->hide();
    ui->sendMoney->hide();
    ui->History->hide();
    ui->GetMoneyWindow->hide();
    ui->accessCard->show();
}

void accessWindow::on_balance_clicked()
{
    socket->sendCommand("component:Accounts,command:Balance,number:"+_cardNum+",pin:"+_pin);
    socket->waitForReadyRead();
    QByteArray answer = socket->getData();
    socket->disconnectFromHost();
    if(!answer.isEmpty()){
        parseAnswer(answer);
        if(!isSuccessed())
            return;
    }else{
        showError("Server is unavailable!\nTry again later.");
        return;
    }
    QString balance = _answer.find("Balance").value();
    ui->cBalance->setAlignment(Qt::AlignCenter);
    ui->cBalance->setText(balance);
    ui->showBalance->show();
}

void accessWindow::on_return_2_clicked()
{
    ui->showBalance->hide();
    ui->cBalance->clear();
}

void accessWindow::on_toSendMoney_clicked()
{
    ui->Card2->setFocus();
    ui->sendMoney->show();
}

void accessWindow::on_return_4_clicked()
{
    ui->sendMoney->hide();
    ui->Card2->clear();
    ui->transfAmount->clear();
}

void accessWindow::on_sendButton_clicked()
{
    QString cardNum = ui->Card2->text().replace(" ","");
    QString money = ui->transfAmount->text();

    if(cardNum.isEmpty() || money.isEmpty()){
        showError("Inputs can't be blank!");
        return;
    }

    QMessageBox msgBox;
    QMessageBox::StandardButton res = msgBox.question(this, "Confirmation", "Are you sure that you want to send \n $"
                   +money+" to card \n"
                   +cardNum+" ?");

    if(res == QMessageBox::Yes){
        socket->sendCommand("component:Accounts,command:Transfer,numberFrom:"+_cardNum+",pin:"+_pin+",numberTo:"+cardNum+",amount:"+money);
        socket->waitForReadyRead();
        QByteArray answer = socket->getData();
        socket->disconnectFromHost();
        if(!answer.isEmpty()){
            parseAnswer(answer);
            if(!isSuccessed())
                return;
        }else{
            showError("Server is unavailable!\nTry again later.");
            return;
        }

        QMessageBox msgBox;
        msgBox.information(this, "Success", "Successful transaction");

        on_return_4_clicked();
    }
}

void accessWindow::on_return_3_clicked()
{
    ui->GetMoneyWindow->hide();
    ui->main->show();
}

void accessWindow::on_toGetMoney_clicked()
{
    ui->GetMoneyWindow->show();
    ui->showBalance->hide();
    ui->main->hide();
}

void accessWindow::on_getMoney_clicked()
{
    QObjectList radio = ui->radioButtons->children();
    QRadioButton *checked = 0;
    for(int i=0; i<radio.length(); ++i){
        QRadioButton *current = qobject_cast<QRadioButton*>(radio[i]);
        if(current->isChecked())
            checked = current;
    }
    if(checked == 0){
        showError("You didn't choose amount!");
        return;
    }
    getMoney(checked->text());
    checked->setAutoExclusive(false);
    checked->setChecked(false);
    checked->setAutoExclusive(true);
}

void accessWindow::getMoney(const QString& amount){

    socket->sendCommand("component:Accounts,command:Withdrawal,number:"+_cardNum+",pin:"+_pin+",amount:"+amount);
    socket->waitForReadyRead();
    QByteArray answer = socket->getData();
    socket->disconnectFromHost();
    if(!answer.isEmpty()){
        parseAnswer(answer);
        if(!isSuccessed())
            return;
    }else{
        showError("Server is unavailable!\nTry again later.");
        return;
    }

    QWidget *img = new QWidget(0, Qt::FramelessWindowHint);
    img->resize(227, 96);
    img->move(this->x()+this->width()/2-113, this->y()+this->height()-96);
    QImage dollar;
    dollar.load("dollar.jpg");
    QLabel *myLabel = new QLabel(img);
    myLabel->setPixmap(QPixmap::fromImage(dollar));
    img->show();

    QPropertyAnimation animation(img, "geometry");
    animation.setDuration(800);
    animation.setEndValue(QRect(this->x()+this->width()/2-113, this->y()+317, img->width(), img->height()));

    animation.start();

    QMessageBox msgBox;
    msgBox.information(this, "Success", "Successful transaction");
    on_return_3_clicked();
    img->hide();
}

void accessWindow::on_toGetMoney_2_clicked()
{
    on_toGetMoney_clicked();
}

void accessWindow::on_otherAmount_clicked()
{
    int amount = QInputDialog::getInt(this, "Withdrawal amount", "Withdrawal amount: ", 0, 1);
    if(amount != 0)
        getMoney(QString::number(amount));
}

void accessWindow::on_toHistory_clicked()
{
    QStringListModel *model = new QStringListModel(this);
    QStringList list;

    socket->sendCommand("component:Accounts,command:History,number:"+_cardNum+",pin:"+_pin);
    socket->waitForReadyRead();
    QByteArray answer = socket->getData();
    socket->disconnectFromHost();
    if(!answer.isEmpty()){
        parseAnswer(answer);
        if(!isSuccessed())
            return;
    }else{
        showError("Server is unavailable!\nTry again later.");
        return;
    }

    int amountOfActions = _answer.find("size").value().toInt();

    if(amountOfActions == 0)
        list << "" << "You haven't done any transaction yet!";

    for(int i=1; i <= amountOfActions; ++i){
        // elements[0] - destination card number
        // elements[1] - date
        // elements[2] - amount
        QStringList elements = _answer.find("transaction-"+QString::number(i)).value().split('|');
        list << "------------------------------------------------------------------------------"
             << elements[1];
        if(elements[0] == "0"){
            list << "Withdraw: $ " + elements[2];
        }else{
            list << "Transaction: $ " + elements[2]
                 << "to card " + elements[0];
        }
    }

    model->setStringList(list);
    ui->listView->setModel(model);
    ui->History->show();
}

void accessWindow::on_return_6_clicked()
{
    ui->History->hide();
}
