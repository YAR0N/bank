#ifndef ACCOUNTMANAGER_H
#define ACCOUNTMANAGER_H

#include <string>
#include "BaseManager.h"

class AccountManager: public BaseManager {

public:

    AccountManager(Request &req):
            BaseManager(req)
    {
    }


    virtual void makeResponce();

private:

    void checkBalance();
    void withdrawal();
    void transfer();
    void history();

    static const std::string numberC;
    static const std::string pinC;
    static const std::string amountC;
    static const std::string numberFromC;
    static const std::string numberToC;

    static const char* currentTime();

};

#endif