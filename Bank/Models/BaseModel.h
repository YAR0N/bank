#ifndef BASEMODEL_H
#define BASEMODEL_H

#include "SQLiteCpp.h"

template <class Model>
class BaseModel{

public:

    static Model* model(){
        return new Model;
    }

    bool save() const {
        return doSave();
    }

    bool update() const {
        return doUpdate();
    }

    virtual ~BaseModel(){};

protected:

    SQLite::Database* connectToDb() const {
        return new SQLite::Database(dbName, SQLITE_OPEN_READWRITE);
    }

private:

    virtual bool doSave() const = 0;
    virtual bool doUpdate() const = 0;

    BaseModel& operator=(const BaseModel&);

    static const std::string dbName;
};

template <class M>
const std::string BaseModel<M>::dbName = "bank.db";

#endif