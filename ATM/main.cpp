#include "accesswindow.h"
#include <QApplication>
#include <QLabel>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    int port = atoi(argv[2]);
    accessWindow w(argv[1], quint16(port));
    w.show();

    return a.exec();
}
