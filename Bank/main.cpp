#include <iostream>
#include <signal.h>
#include "socketUtils.h"
#include "threadsUtils.h"
#include "SQLiteCpp.h"

void exitSignalHandle(int sign);
void initDB();

int socketI = -1;

int main(int agrc, char** arg) {

    initDB();

    signal(SIGINT, exitSignalHandle);

    ThreadsManager tm(3, requestSignal);
    for (int i = 0; i < 3; ++i)
    {
        tm.add(tcpHandle);
    }

    int port = 8081;
    if(agrc > 1)
        port = atoi(arg[1]);

    socketI = tcpSocket(port);

    if (socketI != -1) {
        {
            std::lock_guard<std::mutex> lockl(coutLock);
            std::cout<<"Port: "<<port<<std::endl;
        }
        tcpAccept(socketI);
    }

    return 0;
}

void exitSignalHandle(int sign)
{
    endAccept = true;
    if(socketI != -1)
        close(socketI);
}

void initDB()
{
     SQLite::Database db("bank.db", SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE);
     if (!db.tableExists("accounts")) {
        db.exec("CREATE TABLE accounts (id INTEGER PRIMARY KEY, number STRING, pin STRING, clientName STRING, amount DOUBLE)");
     }
    if (!db.tableExists("transactions")) {
        db.exec("CREATE TABLE transactions (id INTEGER PRIMARY KEY, numberFrom STRING, numberTo STRING, date STRING, amount DOUBLE)");
    }
}