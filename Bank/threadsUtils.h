// 1234
#include <dirent.h>
#include <thread>

class ThreadsManager {
private:
    int maxThreads;
    int currentThreads;

    std::vector<std::thread*> threats;
    std::condition_variable& waitCond;

    bool stopI;

    ThreadsManager(const ThreadsManager&);
    ThreadsManager& operator=(const ThreadsManager&);

public:

    ThreadsManager(int maxThreads, std::condition_variable &wait) :
            maxThreads(maxThreads),
            currentThreads(0),
            stopI(false),
            threats(static_cast<size_t >(maxThreads)),
            waitCond(wait)
    {

    }

    ~ThreadsManager()
    {
        stopI = true;
        waitCond.notify_all();
        if(currentThreads != 0) {
            for (int i=0; i<currentThreads; ++i){
                threats[i]->join();
            }
        }

    }

    int add(void (*f)(const bool&))
    {
        if (currentThreads < maxThreads) {
            threats[currentThreads] = new std::thread(f, std::ref(stopI));
            return currentThreads++;
        } else {
            return -1;
        }
    }
};