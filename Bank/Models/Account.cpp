#include "Account.h"

const std::string Account::findSQL = "SELECT * FROM accounts WHERE number = :num";
//const std::string Account::insertSQL = "INSERT INTO accounts VALUES(? ? ? ?)";
const std::string Account::updateSQL = "UPDATE accounts SET number = :number, pin = :pin, clientName = :clientName, amount = :amount WHERE id = :id";

Account * Account::findByNumber(const std::string &number) {
    SQLite::Database* db = connectToDb();
    SQLite::Statement st(*db, findSQL);

    st.bind(":num", number);

    if(st.executeStep()) {
        Account* ac = new Account(st.getColumn(1), st.getColumn(2), st.getColumn(3), st.getColumn(4));
        ac->_id = st.getColumn(0);
        return ac;
    } else {
        return 0;
    }
}

bool Account::doUpdate() const {
    SQLite::Database* db = connectToDb();
    SQLite::Statement st(*db, updateSQL);

    st.bind(":number", _number);
    st.bind(":pin", _pin);
    st.bind(":clientName", _clientName);
    st.bind(":amount", _amount);
    st.bind(":id", _id);

    return st.exec();
}

bool Account::doSave() const{
    return false;
}