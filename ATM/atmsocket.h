#ifndef ATMSOCKET_H
#define ATMSOCKET_H

#include <QObject>
#include <QTcpSocket>
#include <QString>

class ATMSocket : public QObject
{
    Q_OBJECT

public:

    explicit ATMSocket(const QString& ip, const quint16& port, QObject *parent = 0);
    ~ATMSocket();

    inline QString getIP() const {return _serverIP;}
    inline quint16 getPort() const {return _serverPort;}

    inline QByteArray getData(){return _data;}
    void waitForReadyRead(int msec = 5000);

public slots:

    void connected();
    void disconnected();
    void bytesWritten(qint64 bytes);
    void readyRead();

    void sendCommand(const QString& command);

    bool connectToHost();
    void disconnectFromHost();
private:

    QString _serverIP;
    quint16 _serverPort;

    QByteArray _data;

    QTcpSocket *tcpSocket;

    ATMSocket (const ATMSocket&);
    ATMSocket operator = (const ATMSocket&);
};

#endif // ATMSOCKET_H
