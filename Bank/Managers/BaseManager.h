#ifndef BASEMANAGER_H
#define BASEMANAGER_H 

#include "../Request.h"

class BaseManager {

public:


    BaseManager(Request& req) :
            _req(req)
    {
    }

    virtual ~BaseManager(){}

    virtual void makeResponce() = 0;

protected:

    Request& _req;

};

#endif