#include <iomanip>
#include "AccountManager.h"
#include "../Models/Account.h"
#include "../Models/AccountTransaction.h"

const std::string AccountManager::numberC = "number";
const std::string AccountManager::pinC = "pin";
const std::string AccountManager::amountC = "amount";
const std::string AccountManager::numberFromC = "numberFrom";
const std::string AccountManager::numberToC = "numberTo";

const char* AccountManager::currentTime() {
    char* tm = new char[80];
    time_t now = time(0);
    strftime(tm, 80, "%d.%m.%Y %H-%M-%S", localtime(&now));
    return tm;
}

void AccountManager::makeResponce(){
    std::string command;
    if(_req.getRequestParameter("command", command)) {
        if(command == "Balance")
            checkBalance();
        else if(command == "Withdrawal")
            withdrawal();
        else if(command == "Transfer")
            transfer();
        else if(command == "History")
            history();
        else
            _req.formError("No such command");
    } else {
        _req.formError("No command parameter");
    }
}

void AccountManager::checkBalance(){
    try{
        std::string number, pin;
        if(_req.getRequestParameter(numberC, number) && _req.getRequestParameter(pinC, pin)) {
            Account* ac = Account::model()->findByNumber(number);
            if(ac == 0 || ac->get_pin() != pin)
                _req.formError("Wrong account details");
            else {
                _req.formSuccess();
                std::ostringstream strs;
                strs<<std::setprecision(2)<<std::fixed<<ac->get_amount();
                _req.setResponseParameter("Balance", strs.str());
            }
        } else {
            _req.formError("Not all parameters");
        }
    } catch (std::exception& e) {
        std::cerr<<e.what()<<std::endl;
        _req.formError("Server error");
    }
}

void AccountManager::withdrawal() {
    try{
        std::string number, pin, amount;
        if(_req.getRequestParameter(numberC, number) && _req.getRequestParameter(pinC, pin) && _req.getRequestParameter(amountC, amount)) {
            Account* ac = Account::model()->findByNumber(number);
            double amountD = atof(amount.data());
            if(ac == 0 || ac->get_pin() != pin)
                _req.formError("Wrong account details");
            else if(amountD > ac->get_amount() || amountD <= 0)
                _req.formError("You are not allowed to withdraw this amount");
            else{
                ac->set_amount(ac->get_amount() - amountD);
                ac->update();
                AccountTransaction ts(ac->get_number(), "0", AccountManager::currentTime(), amountD);
                ts.save();
                _req.formSuccess();
                std::ostringstream strs;
                strs<<std::setprecision(2)<<std::fixed<<ac->get_amount();
                _req.setResponseParameter("Balance", strs.str());
            }
        } else {
            _req.formError("Not all parameters");
        }
    } catch (std::exception& e) {
        std::cerr<<e.what()<<std::endl;
        _req.formError("Server error");
    }
}

void AccountManager::transfer() {
    try{
        std::string numberFrom, numberTo, pin, amount;
        if(_req.getRequestParameter(numberFromC, numberFrom) && _req.getRequestParameter(numberToC, numberTo)
                && _req.getRequestParameter(pinC, pin) && _req.getRequestParameter(amountC, amount))
        {
            Account* from = Account::model()->findByNumber(numberFrom);
            Account* to = Account::model()->findByNumber(numberTo);
            double amountD = atof(amount.data());
            if(from == 0 || to == 0 || from->get_pin() != pin)
                _req.formError("Wrong account details");
            else if (amountD > from->get_amount() || amountD <= 0)
                _req.formError("You are not allowed to transfer this amount");
            else {
                from->set_amount(from->get_amount() - amountD);
                to->set_amount(to->get_amount() + amountD);
                from->update();
                to->update();
                AccountTransaction ts(from->get_number(), to->get_number(), AccountManager::currentTime(), amountD);
                ts.save();
                _req.formSuccess();
                std::ostringstream strs;
                strs<<std::setprecision(2)<<std::fixed<<from->get_amount();
                _req.setResponseParameter("Balance", strs.str());
            }
        } else {
            _req.formError("Not all parameters");
        }
    } catch (std::exception& e) {
        std::cerr<<e.what()<<std::endl;
        _req.formError("Server error");
    }
}

void AccountManager::history() {
    try{
        std::string number, pin;
        if(_req.getRequestParameter(numberC, number) && _req.getRequestParameter(pinC, pin)) {
            Account* ac = Account::model()->findByNumber(number);
            if(ac == 0 || ac->get_pin() != pin)
                _req.formError("Wrong account details");
            else {
                AccountTransaction::model()->findAllByAccountAddToResponce(number, _req);
            }
        } else {
            _req.formError("Not all parameters");
        }
    } catch (std::exception& e) {
        std::cerr<<e.what()<<std::endl;
        _req.formError("Server error");
    }
}