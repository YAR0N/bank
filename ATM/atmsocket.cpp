#include "atmsocket.h"

ATMSocket::ATMSocket(const QString &ip, const quint16 &port, QObject *parent):
    QObject(parent),
    _serverIP(ip),
    _serverPort(port),
    _data(""),
    tcpSocket(new QTcpSocket)
{
    connect(tcpSocket, SIGNAL(connected()),this, SLOT(connected()));
    connect(tcpSocket, SIGNAL(disconnected()),this, SLOT(disconnected()));
    connect(tcpSocket, SIGNAL(bytesWritten(qint64)),this, SLOT(bytesWritten(qint64)));
    connect(tcpSocket, SIGNAL(readyRead()),this, SLOT(readyRead()));
}


ATMSocket::~ATMSocket()
{
    disconnectFromHost();
}

bool ATMSocket::connectToHost(){
    tcpSocket->connectToHost(_serverIP, _serverPort);
    if(!tcpSocket->waitForConnected(5000))
    {
        qDebug() << "C Error: " << tcpSocket->errorString();
        return false;
    }
    return true;
}

void ATMSocket::disconnectFromHost(){
    if(tcpSocket->state() == QTcpSocket::ConnectedState)
        tcpSocket->disconnectFromHost();
}

void ATMSocket::sendCommand(const QString& command){
    if(connectToHost())
        tcpSocket->write( command.toLocal8Bit().data(), command.length() );
}

void ATMSocket::waitForReadyRead(int msec){
    if ( !tcpSocket->waitForReadyRead(msec)) {
        qDebug() << "W Error: " << tcpSocket->errorString();
        return;
    }
}

void ATMSocket::connected()
{
    qDebug() << "connected...";
}

void ATMSocket::disconnected()
{
    qDebug() << "disconnected...";
}

void ATMSocket::bytesWritten(qint64 bytes)
{
    qDebug() << bytes << " bytes written...";
}

void ATMSocket::readyRead()
{
    qDebug() << "reading...";
    _data = tcpSocket->readAll();
}
