#-------------------------------------------------
#
# Project created by QtCreator 2014-11-09T13:43:08
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ATM
TEMPLATE = app


SOURCES += main.cpp\
        accesswindow.cpp \
    atmsocket.cpp

HEADERS  += accesswindow.h \
    atmsocket.h

FORMS    += accesswindow.ui
