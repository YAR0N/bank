#ifndef REQUEST_H
#define REQUEST_H 

#include <map>
#include <regex>
#include <iostream>


class Request {

public:

    ~Request(){}

    static Request* getRequest(const ssize_t& n, const char* data) {
        return new Request(n, data);
    }

    bool getRequestParameter(const std::string& parameter, std::string& result) const;
    Request* setResponseParameter(const std::string parameter, const std::string& data);
    void formError(const std::string& reason);
    void formSuccess();

    const ssize_t getResponce(char** st);

private:

    std::map<std::string, std::string> _requestData;
    std::map<std::string, std::string> _responseData;

    Request(const ssize_t& n, const char* data):
            _requestData(),
            _responseData()
    {
        parseRequest(n, data);
    }

    bool parseRequest(const ssize_t& n, const char* data);
    const ssize_t formResponceString(char** st) const;
};


#endif
