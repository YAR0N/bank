#ifndef ACCESSWINDOW_H
#define ACCESSWINDOW_H

#include <QMainWindow>
#include "atmsocket.h"

namespace Ui {
class accessWindow;
}

class accessWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit accessWindow(const QString& ip, const quint16 &port, QWidget *parent = 0);
    ~accessWindow();

private slots:
    void on_pushButton_clicked();
    void on_exit_clicked();
    void on_balance_clicked();
    void on_return_2_clicked();
    void on_toGetMoney_clicked();
    void on_pushButton_2_clicked();
    void on_toSendMoney_clicked();
    void on_return_4_clicked();
    void on_sendButton_clicked();
    void on_return_3_clicked();
    void on_getMoney_clicked();
    void on_toGetMoney_2_clicked();
    void on_otherAmount_clicked();
    void on_toHistory_clicked();

    void on_return_6_clicked();

private:
    Ui::accessWindow *ui;
    ATMSocket *socket;

    QString _cardNum;
    QString _pin;

    QMap<QString, QString> _answer;

    accessWindow (const accessWindow&);
    accessWindow& operator = (const accessWindow&);

    void parseAnswer(const QByteArray &answer);
    void showError(const QString &text);
    bool isSuccessed();
    void getMoney(const QString &amount);
};

#endif // ACCESSWINDOW_H
