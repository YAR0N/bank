#include <iomanip>
#include "AccountTransaction.h"

const std::string AccountTransaction::insertSQL = "INSERT INTO transactions(numberFrom, numberTo, date, amount) VALUES(:from, :to, :dt, :am)";
const std::string AccountTransaction::findAllSQL = "SELECT * FROM transactions WHERE numberFrom = :num ORDER BY id DESC LIMIT 5";


bool AccountTransaction::doUpdate() const {
    return false;
}

bool AccountTransaction::doSave() const {
    SQLite::Database* db = connectToDb();
    SQLite::Statement st(*db, insertSQL);

    st.bind(":from", _numberFrom);
    st.bind(":to", _numberTo);
    st.bind(":dt", _date);
    st.bind(":am", _amount);

    return st.exec();
}

void AccountTransaction::findAllByAccountAddToResponce(const std::string& number, Request& req) const {
    SQLite::Database* db = connectToDb();
    SQLite::Statement st(*db, findAllSQL);

    st.bind(":num", number);
    req.formSuccess();

    std::string res;
    int i = 1;
    for (; st.executeStep(); ++i) {
        std::ostringstream strs;
        strs<<std::setprecision(2)<<std::fixed<<st.getColumn(4);
        res = st.getColumn(2).operator const std::string()+"|"+st.getColumn(3).operator const std::string()+"|"+strs.str();
        req.setResponseParameter("transaction-"+std::to_string(i), res);
    }

    req.setResponseParameter("size", std::to_string(i-1));
}