#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <unistd.h>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <string.h>
#include "Request.h"

std::queue<int> socketRequests;
std::mutex requestLock;
std::condition_variable requestSignal;

std::mutex coutLock;

bool endAccept = false;

int tcpSocket(const int port) {

    int socketI = -1;
    struct sockaddr_in serverAddr;

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = INADDR_ANY;
    serverAddr.sin_port = htons(port);

    socketI = socket(AF_INET, SOCK_STREAM, 0);
    if (socketI < 0) {
        std::cerr<<"Socket creation error"<<std::endl;
        return -1;
    }

    if(bind(socketI, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0) {
        std::cerr<<"Socket bind error"<<std::endl;
        close(socketI);
        return -1;
    }

    if(listen(socketI, 9) < 0) {
        std::cerr<<"Socket Listen error"<<std::endl;
        close(socketI);
        return -1;
    }

    return socketI;
}

void tcpAccept(const int socketI) {

    int clientI = -1;
    struct sockaddr_in clientAddr;

    socklen_t clientAddrSize = sizeof(clientAddr);

    while (!endAccept) {
        clientI = accept(socketI, (struct sockaddr *)&clientAddr, &clientAddrSize);
        if(clientI != -1) {
            std::unique_lock<std::mutex> lock1(requestLock);
            socketRequests.push(clientI);
            requestSignal.notify_one();
        } else {
            std::cerr<<"Socket accept error"<<std::endl;
        }
    }
    std::cerr<<"Socket closed"<<std::endl;
}

void tcpHandle(const bool& tEnd) {

    {
        std::lock_guard<std::mutex> lockl(coutLock);
        std::cout<<"Thread started"<<std::endl;
    }

    int clientI;

    size_t requestSize = 1024  ;
    ssize_t realSize = 0;
    char data[requestSize];

    while (!tEnd) {
        {
            std::unique_lock<std::mutex> lock1(requestLock);
            requestSignal.wait(lock1);

            if(socketRequests.empty()) continue;

            clientI = socketRequests.front();
            socketRequests.pop();
        }

        bzero(data, requestSize);
        realSize = recv(clientI, data, requestSize, 0);

        if(realSize > 0) {
            {
                std::lock_guard<std::mutex> lockl(coutLock);
                std::cout<<data<<std::endl;
            }
            Request* req = Request::getRequest(realSize, data);
            char* res = 0;
            ssize_t resS = req->getResponce(&res);
          
            write(clientI, res, resS);
            close(clientI);
        }


    }
    {
        std::lock_guard<std::mutex> lockl(coutLock);
        std::cout<<"Thread stopped"<<std::endl;
    }
}